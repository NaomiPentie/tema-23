

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
 const $ = require('jquery');
 require('bootstrap');
 import Vue from 'vue';
 import BootstrapVue from 'bootstrap-vue';
 import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
 Vue.use(BootstrapVue);
 var newCounter = new Vue({
    el: '#count',
    delimiters: ['{*', '*}'],
    data: {
     number: 0,
    },
    methods: {
        countup () {
            this.number++;

        },
        countdown() {
            this.number--;
        },
        reset() {
            this.number = 0;
        }
    }

});

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');
