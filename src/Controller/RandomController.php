<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RandomController extends AbstractController
{
    /**
     * @Route("/random", name="random")
     */
        public function compareRandomNumber(){
    
            $url=$this->generateUrl('random');
            // function getRandomInt(min, max) {
            //     min = Math.ceil(min);
            //     max = Math.floor(max);
            //     return Math.floor(Math.random() * (max - min + 1)) + min;
            // }
            $randomNumber   = rand(0, 50);
            $insertedNumber = '';
    
            if (isset($_POST['submit'])) {
                $insertedNumber = $_POST[ 'insertedNumber'];
                if ($insertedNumber > $randomNumber) {
                        echo " Numarul introdus este mai mare decat cel ales random care este " . $randomNumber . ".";
                    } elseif ( $insertedNumber < $randomNumber) {
                        echo " Numarul introdus este mai mic decat cel ales random care este " . $randomNumber . ".";
                    } elseif ( $insertedNumber == $randomNumber) {
                        echo " Numarul introdus este identic cu cel ales random care este " . $randomNumber . "." ;
                    } 
            }
            // function getRandomizer(bottom, top) {
            //     return function() {
            //         return Math.floor( Math.random() * ( 1 + top - bottom ) ) + bottom;
            //     }
            // }
            return $this->render('random/index.html.twig',['url'=>$url]);
        }
}
